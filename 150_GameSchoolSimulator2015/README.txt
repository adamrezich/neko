===========================
Game School Simulator 2015
===========================

Done by: Neko Means Cat

Members:
  Adam Rezich (adam.rezich)
  Eduardo Gorinstein (eduardo.gorinstein)
  Samuel Valdez (samuel.v)
  Tai Der Hui (tai.derhui)
  
=========================
Installation Instructions
=========================
Run the included setup.exe file in the installation folder. Follow the on-screen installation instructions until the process is complete

===========
How to Play
===========
Objective: Make your school a world-famous school!

Spend money to construct new rooms, or upgrade existing ones.

Manage different variables such as acceptance GPA or student tuition rates to
get more students or get more tuition per month.

Get good students to graduate to gain 'rep'. If a student's motivation reaches 0,
or his GPA falls too low, he will drop out, causing you to lose a student and also
lose some 'rep'.

Once you gain a 500 'rep', you win! If you drop below $100,000 though, you'll be too far in debt to recover!

========
Controls
========
Most controls in game are mouse-based. 

You need to use the keyboard to enter the name of your school.

There are also some keyboard shortcuts once you're in the game:
SPACE: Build Mode
ESCAPE: Open options menu and pause the game.
M: Brings up the management screen

Camera Controls:
Click and drag or use the left and right arrow keys to pan left and right.
Use the mouse wheel or use the up and down arrow keys to zoom in and out.

===========
Cheat Codes
===========
Press CTRL + SHIFT + 4 to get a bunch of money

=======
Credits
=======
President: Claude Comair
Instructor: Jason Tartaglia

Lead Artist: Patrick-Michael Casey
Lead Sound Designer: Spencer Mauro

Playtesters:
  - Garrett Huxatable
  - Rebecca Lepore
  - Spencer Mauro
  - Joshua Louderback
  - Steven Gallwas
  - And all other playtesters who helped make our game better!

Student "Oh" voice recordings:
Cheong Ming Wei
Hannah Tan
Joyc Leong
Ng Wan Qing

Audio engine : FMOD Sound System by Firelight Technologies