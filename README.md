NEKO ENGINE
===========

This is the NEKO ENGINE, a game engine written in C, intended for use with DigiPen Institute of Technology's "Alpha Engine."


Etymology
---------

"Neko" means "cat."


Features
--------

* Component-based engine architecture to facilitate rapid development
* Incredibly basic vector math
* Doubly-linked lists (of void pointers)
* Action lists
* Vectors (dynamically-size arrays of void pointers)
* Style guide


Notes
-----

Put data files in an organized manner in "Game School Simulator 2015/data," and it'll be *magically* turned into `pak0.pak` when you compile the game!