/******************************************************************************
Filename: managescreen.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_MANAGESCREEN__
#define __ARCH_MANAGESCREEN__

#include "entity.h"
#include "hash.h"

#define ARCH_MANAGESCREEN HASH("ARCH_MANAGESCREEN")
void arch_manageScreen(ENTITY *entity);

#endif
