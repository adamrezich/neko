/******************************************************************************
Filename: moneyinfo.h

Project Name: Game School Simulator 2015

Author: Eduardo Gorinstein

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_MONEYINFO__
#define __ARCH_MONEYINFO__

#include "entity.h"
#include "hash.h"

#define ARCH_MONEYINFO HASH("ARCH_MONEYINFO")
void arch_moneyInfo(ENTITY *entity);

#endif
