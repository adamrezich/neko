/******************************************************************************
Filename: background.h

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_BACKGROUND_H__
#define __ARCH_BACKGROUND_H__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_BACKGROUND HASH("ARCH_BACKGROUND")

void arch_background(ENTITY *);

#endif