/******************************************************************************
Filename: roomactor.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_ROOMACTOR__
#define __ARCH_ROOMACTOR__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_ROOMACTOR HASH("ARCH_ROOMACTOR")

void arch_roomActor(ENTITY *);

#endif