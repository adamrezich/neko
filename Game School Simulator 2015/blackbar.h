/******************************************************************************
Filename: blackbar.h

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_BLACKBAR_H__
#define __ARCH_BLACKBAR_H__

#include "entity.h"
#include "hash.h"

#define ARCH_BLACKBAR HASH("ARCH_BLACKBAR")

void arch_blackbar(ENTITY *entity);

#endif