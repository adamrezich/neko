/******************************************************************************
Filename: newsfeed.h

Project Name: Game School Simulator 2015

Author: Eduardo Gorinstein

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_NEWSFEED__
#define __ARCH_NEWSFEED__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_NEWSFEED HASH("ARCH_NEWSFEED")

void arch_newsFeed(ENTITY *);

#endif