/******************************************************************************
Filename: UI_manage.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_UIMANAGE__
#define __ARCH_UIMANAGE__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_UIMANAGE HASH("ARCH_UIMANAGE")

void arch_uimanage(ENTITY *);

#endif