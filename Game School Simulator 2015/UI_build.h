/******************************************************************************
Filename: UI_build.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_UIBUILD__
#define __ARCH_UIBUILD__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_UIBUILD HASH("ARCH_UIBUILD")

void arch_uibuild(ENTITY *);

#endif