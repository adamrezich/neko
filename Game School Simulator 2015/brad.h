/******************************************************************************
Filename: brad.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_BRAD__
#define __ARCH_BRAD__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_BRAD HASH("ARCH_BRAD")

void arch_brad(ENTITY *);

#endif