/******************************************************************************
Filename: splash.h

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_SPLASH__
#define __ARCH_SPLASH__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_SPLASH HASH("ARCH_SPLASH")

void arch_splash(ENTITY *);

#endif