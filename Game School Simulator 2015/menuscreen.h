/******************************************************************************
Filename: menuscreen.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_MENUSCREEN__
#define __ARCH_MENUSCREEN__

#include "entity.h"
#include "hash.h"

#define ARCH_MENUSCREEN HASH("ARCH_MENUSCREEN")
void arch_menuScreen(ENTITY *entity);

#endif
