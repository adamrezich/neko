/******************************************************************************
Filename: ghostroom.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_GHOSTROOM__
#define __ARCH_GHOSTROOM__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_GHOSTROOM HASH("ARCH_GHOSTROOM")

void arch_ghostRoom(ENTITY *);

#endif