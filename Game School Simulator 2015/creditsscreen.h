/******************************************************************************
Filename: creditsscreen.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_CREDITSSCREEN__
#define __ARCH_CREDITSSCREEN__

#include "entity.h"
#include "hash.h"

#define ARCH_CREDITSSCREEN HASH("ARCH_CREDITSSCREEN")
void arch_creditsScreen(ENTITY *entity);

#endif
