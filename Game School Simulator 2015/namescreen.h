/******************************************************************************
Filename: namescreen.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_NAMESCREEN__
#define __ARCH_NAMESCREEN__

#include "entity.h"
#include "hash.h"

#define ARCH_NAMESCREEN HASH("ARCH_NAMESCREEN")
void arch_nameScreen(ENTITY *entity);

#endif
