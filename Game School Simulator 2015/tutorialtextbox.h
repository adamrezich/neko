/******************************************************************************
Filename: tutorialtextbox.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_TUTORIALTEXTBOX__
#define __ARCH_TUTORIALTEXTBOX__

#include "entity.h"
#include "hash.h"

#define ARCH_TUTORIALTEXTBOX HASH("ARCH_TUTORIALTEXTBOX")
void arch_tutorialTextBox(ENTITY *entity);

#endif
