/******************************************************************************
Filename: menuscreenlogic.h

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __COMP_MENUSCREENLOGIC__
#define __COMP_MENUSCREENLOGIC__

#include "entity.h"
#include "hash.h"
#include "actionlist.h"

#define COMP_MENUSCREENLOGIC HASH("COMP_MENUSCREENLOGIC")

typedef struct {
  bool pressedStart;
  bool beganFading;
  ALIST actions;
} CDATA_MENUSCREENLOGIC;

void comp_menuScreenLogic_logicUpdate(COMPONENT *self, void *event);
void comp_menuScreenLogic(COMPONENT *self);
void comp_menuScreenLogic_initialize(COMPONENT *self, void *event);
void comp_menuScreenLogic_destroy(COMPONENT *self, void *event);

#endif
