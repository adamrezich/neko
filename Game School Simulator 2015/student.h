/******************************************************************************
Filename: student.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_STUDENT__
#define __ARCH_STUDENT__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_STUDENT HASH("ARCH_STUDENT")

void arch_student(ENTITY *);

#endif