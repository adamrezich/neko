/******************************************************************************
Filename: studentactor.h

Project Name: Game School Simulator 2015

Author: Tai Der Hui

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_STUDENTACTOR__
#define __ARCH_STUDENTACTOR__

#include "entity.h"
#include "hash.h"

#define ARCH_STUDENTACTOR HASH("ARCH_STUDENTACTOR")

void arch_studentActor(ENTITY *entity);

#endif
