/******************************************************************************
Filename: inspectionscreen.h

Project Name: Game School Simulator 2015

Author: Samuel Valdez

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_INSPECTIONSCREEN__
#define __ARCH_INSPECTIONSCREEN__

#include "entity.h"
#include "hash.h"

#define ARCH_INSPECTIONSCREEN HASH("ARCH_INSPECTIONSCREEN")
void arch_inspectionScreen(ENTITY *entity);

#endif
