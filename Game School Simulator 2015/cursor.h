/******************************************************************************
Filename: cursor.h

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#ifndef __ARCH_CURSOR_H__
#define __ARCH_CURSOR_H__

#include "../NekoEngine/entity.h"
#include "../NekoEngine/hash.h"

#define ARCH_CURSOR HASH("ARCH_CURSOR")

void arch_cursor(ENTITY *);

#endif