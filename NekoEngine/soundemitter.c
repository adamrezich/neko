/******************************************************************************
Filename: soundemitter.c

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include <stdlib.h>
#include <string.h>
#include "component.h"
#include "entity.h"
#include "transform.h"
#include "vectormath.h"
#include "util.h"
#include "../AlphaEngine/AEEngine.h"

void comp_soundEmitter_initialize(COMPONENT *self, void *event) {
}

void comp_soundEmitter_frameUpdate(COMPONENT *self, void *event) {
}

void comp_soundEmitter_destroy(COMPONENT *self, void *event) {
}

void comp_soundEmitter(COMPONENT *self) {
}