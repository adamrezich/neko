/******************************************************************************
Filename: neko.c

Project Name: Game School Simulator 2015

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include "neko.h"
#include "game.h"
#include "space.h"
#include "util.h"
#include "../AlphaEngine/AEEngine.h"

#pragma comment(lib, "../lib/Alpha_Engine.lib")
#pragma comment(lib, "../lib/fmodex_vc.lib")

#if _CHECK_LEAKS
#pragma comment (lib, "C:\\Program Files (x86)\\Visual Leak Detector\\lib\\Win32\\vld.lib")
#include <vld.h>
#endif