Roadmap
=======

Phase one
---------

* students
* placing buildings
* passage of time
* adjusting basic school parameters (tuition, GPA)
* incoming students
* reputation
* money/debt systems


Phase two
---------

* degree/courses
* room upgrades

Phase three
-----------

* teachers
* random events
